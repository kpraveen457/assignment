const express = require("express");
const request = require("request");
const config = require("config");
const router = express.Router();
const auth = require("../middleware/auth");
const { check, validationResult } = require("express-validator");

const Article = require("../models/Article");
const User = require("../models/User");

// @route    POST article
// @desc     Create article
// @access   Private
router.post(
  "/",
  [
    auth,
    [
      check("title", "Title is required").not().isEmpty(),
      check("body", "Body is required").not().isEmpty(),
      check("author", "Author is required").not().isEmpty(),
    ],
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { title, body, author } = req.body;

    // Build article object

    article = new Article({
      title,
      body,
      author,
    });

    article.user = req.user.id;

    try {
      await article.save();

      res.status(201).json({ msg: "new article created" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

// @route    GET article
// @desc     get all article
// @access   Public

router.get("/", async (req, res) => {
  const sort = {};
  const { page = 1, limit = 10 } = req.query;

  if (req.query.sortBy && req.query.OrderBy) {
    sort[req.query.sortBy] = req.query.OrderBy === "desc" ? -1 : 1;
  }

  try {
    const articles = await Article.find()
      .limit(limit * 1)
      .skip((page - 1) * limit)
      .sort(sort)
      .exec();

    // get total documents in the article collection
    const count = await Article.countDocuments();

    // return response with article, total pages, and current page
    res.json({
      articles,
      totalPages: Math.ceil(count / limit),
      currentPage: page,
    });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("Server error");
  }
});

module.exports = router;
