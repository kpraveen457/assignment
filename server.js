const express = require("express");
const connectDB = require("./config/db");

const app = express();

// connect database
connectDB();

// Init middleware
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("API running"));

// define routes
app.use("/register", require("./routes/register"));
app.use("/login", require("./routes/login"));

app.use("/article", require("./routes//article"));

const PORT = process.env.PORT || 5001;

app.listen(PORT, () => console.log(`server started on ${PORT}`));
